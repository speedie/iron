# iron

## What is iron?
iron is a minimal rautafarmi client written in Bash for GNU/Linux. It allows the user to post/recieve messages using the [rautafarmi API](https://donut.gq/rautafarmi/api.php) and it displays the information to the user in a nice format. iron is based on the older [rchat](https://speedie.gq/rchat) rautafarmi client that used messages.txt to get messages.

## Why should I use iron over rchat?
Unlike rchat, iron is minimal in design and only does what you expect a basic client to do. It does not have auto updating, no history, no donate command, no news, no changelog, and no :open. It also brings a much cleaner and improved codebase. Because of this, bugs are less likely and the client is much faster. The user doesn't have to run lines of code they don't need.

Most importantly though, iron does not use the old messages.txt method used by older instances. This also means iron is the first rautafarmi client to use and support the new JSON based rautafarmi API. This API is much faster and will allow compatible instances to push updates without breaking client support.

## Features
- Vim-like
iron much like rchat is Vim like. Just like Vim, iron has different modes which are used in Vim like ways. You can even use Vim to compose your message if you want (using the ':e' command).
- 16 color support
iron supports 16 colors out of the box. There will also be a 256-color patch for terminals that support it as soon as the rchat patch has been ported over.
- 4chan-like green text
iron has 4chan-like green text for messages that start with '>'.
- Support for the new, fast [rautafarmi API](https://donut.gq/rautafarmi/api.php)
iron is the first unofficial Rautafarmi client with support for the new Rautafarmi API. This API is much faster and less likely to break.
- No pointless features instead.
iron does not have pointless features slowing it down. If the user wants a feature that does not exist, they can easily implement it. Patches can be submitted and the user can apply it to their iron copy using the patch coreutil.
- Hackable
Because it is written in Bash, it is highly flexible and hackable allowing it to do many things with minimal effort by the user.

## Modes
iron has three modes - Normal mode, Command mode and Insert mode. When you first load messages, you will be in Normal mode. This mode is simply used for reading and entering another mode. If you choose to enter Command mode, you're able to perform actions to iron such as exiting or composing a message in an editor.

If you press 'i', you'll be in Insert mode. This mode is used to compose messages. This will bring up a little area where you can type your message. If you press enter, your message will be sent. If you want to send messages with multiple lines, press e or :e to open up a file in an editor ($EDITOR).

## Usage
Simply run iron inside a terminal emulator or something like tmux. For iron to work though, it needs to connect to an instance. As of writing this document, there is only one. You can find it [here](https://donut.gq/rautafarmi). Run ':set instance' and then type in the URL of the instance. If you press enter, the instance will be set. This is saved until settings are reset (or ~/.local/share/iron is deleted). To join the instance, type ':j' and press Enter. This will (assuming dependencies are installed and you are connected to the internet) download the API and then print messages to the screen. Here, you are in Normal mode.

From here, you may run commands, or enter Insert mode to compose a message. See 'Commands' or 'Keybinds' for more information.

## Installation
Download it (see 'Download') and run 'make install' as root.

## Download
iron can be downloaded using dev-vcs/git. To do this:
    - cd
	- git clone https://codeberg.org/speedie/iron
	- cd iron

## Commands
- ':q' - Quit iron
- ':l' - Leave the instance
- ':r' - Refresh
- ':e' - Compose a message in $EDITOR (or Vi if none is specified)
- ':reset' - Reset all settings (including instance) to default.
- ':set instance' - Set the instance
- ':set instance default' - Set the instance to the default
- ':set captcha' - Set the captcha.

## Keybinds
- i - Enter insert mode
- r - Refresh messages
- : - Enter command mode
- e - Compose a message in $EDITOR
- q - Leave the instance
- l - Leave the instance
- ZQ - Quit iron
- ZZ - Leave the instance

## Patches
To submit patches, Create a pull request with your patch in [the patches branch](https://codeberg.org/speedie/iron/src/branch/patches) or [send me an email](mailto:speedie@duck.com). Patches can be generated using the 'diff -up' command on a GNU/Linux system.

## License
iron, **along with any contributions made to it (such as all user-submitted patches)** are licensed under the GNU GPLv3 free software license.

## Issues
Submit them in [the Codeberg repository](https://codeberg.org/speedie/iron/issues) or let me know elsewhere such as [email](mailto:speedie@duck.com).
