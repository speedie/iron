## iron documentation
iron provides documentation in the form of a markdown document. This can be used however is deemed necessary, such as with pandoc to automatically write man pages, HTML and other formats.

## Commands
Full list of all commands in iron by default. More may be added through patches available on the website.

- ':q' - Quit iron
- ':l' - Leave the instance
- ':r' - Refresh
- ':e' - Compose a message in $EDITOR (or Vi if none is specified)
- ':reset' - Reset all settings (including instance) to default.
- ':set instance' - Set the instance
- ':set instance default' - Set the instance to the default
- ':set captcha' - Set the captcha.

## Keybinds
Full list of all keybinds in iron

- i - Enter insert mode
- r - Refresh messages
- : - Enter command mode
- e - Compose a message in $EDITOR
- q - Leave the instance
- l - Leave the instance
- ZQ - Quit iron
- ZZ - Leave the instance

## Usage
- iron - Runs iron

If an instance is not set, it must be set before iron can be used.

## More
See iron.md for the rest of the documentation.
